# rtsponlineplayer
### rtsp视频监控接入的vue实现，后端基于nodejs
### 后端代码在对应文件夹中
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
